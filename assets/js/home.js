$(document).ready(function() {
	var request;

	// function for pagination pages (Previous and Next)
	$('.breadcrumb-item').click(function() {
		if($(this).hasClass('active')) {
			return;
		} else {
			var index = $(".breadcrumb-item").index(this);
			if(index == 0) {
				$(this).addClass('active').html('Previous');
				$('.breadcrumb-item').eq(1).removeClass('active').html('<a href="javascript:void(0);">Next</a>');
				$("#second_form").hide();
				$("#first_form").show();
			} else {
				$(this).addClass('active').html('Next');
				$('.breadcrumb-item').eq(0).removeClass('active').html('<a href="javascript:void(0);">Previous</a>');
				$("#first_form").hide();
				$("#second_form").show();
			}
		}
	});

	
	$("#save_btn").click(function() {
		var answers = [];
		$('.select_question').each(function() {
			answers.push($(this).val());
		});

		if (request) {
	        request.abort();
	    }
    
	    request = $.ajax({
	        url: base_url + 'stats/save',
	        type: "post",
	        dataType :"JSON",
	        data: {answers: answers}
	    });

	    // Callback handler that will be called on success
	    request.done(function (response, textStatus, jqXHR) {
	    	if(response.success) {
		        $("#success_message").fadeIn('slow');
		        setTimeout(function() {
		        	location.replace(base_url + "stats");
		        }, 2000);
	    	}
	    });

	    // Callback handler that will be called on failure
	    request.fail(function (jqXHR, textStatus, errorThrown){
	        
	    });

	});

});