$(document).ready(function() {

	var data_answers = [];
	
	if(answers != '') {
		$.each(answers, function(key, val) {
			data_answers.push(val);
		});
	}

	var ctx = document.getElementById('myChart').getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: ['Q: 1', 'Q: 2', 'Q: 3', 'Q: 4', 'Q: 5', 'Q: 6', 'Q: 7', 'Q: 8', 'Q: 9', 'Q: 10'],
	        datasets: [{
	            label: '# of Answers',
	            data: data_answers,
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(128, 128, 128, 0.2)',
	                'rgba(128, 128, 0, 0.2)',
	                'rgba(0, 255, 0, 0.2)',
	                'rgba(165, 42, 42, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255, 99, 132, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(128, 128, 128, 1)',
	                'rgba(128, 128, 0, 1)',
	                'rgba(0, 255, 0, 1)',
	                'rgba(165, 42, 42, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        }
	    }
	});

	var ctx_2 = document.getElementById('myChart_2').getContext('2d');
	var myChart_2 = new Chart(ctx_2, {
	    type: 'pie',
	    data: {
	        labels: ['Q: 1', 'Q: 2', 'Q: 3', 'Q: 4', 'Q: 5', 'Q: 6', 'Q: 7', 'Q: 8', 'Q: 9', 'Q: 10'],
	        datasets: [{
	            label: '# of Answers',
	            data: data_answers,
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(128, 128, 128, 0.2)',
	                'rgba(128, 128, 0, 0.2)',
	                'rgba(0, 255, 0, 0.2)',
	                'rgba(165, 42, 42, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255, 99, 132, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(128, 128, 128, 1)',
	                'rgba(128, 128, 0, 1)',
	                'rgba(0, 255, 0, 1)',
	                'rgba(165, 42, 42, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        }
	    }
	});

});