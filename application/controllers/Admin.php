<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		isLoggedIn();
		$this->load->model('Stats_model', 'stats');
	}

	public function index()
	{
		$data = [];
		$is_admin = $this->session->userdata('ci_seesion_key')['is_admin'];

		if($is_admin != 'Y') {
			redirect('/');
		}

		$list_of_users = $this->stats->get_list_of_users();

		$data['title'] = "Admin";
		$data['list_of_users'] = $list_of_users;
		$this->load->view('admin', $data);
	}
}
