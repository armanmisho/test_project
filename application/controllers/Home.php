<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		isLoggedIn();
		$this->load->model('Stats_model', 'stats');
	}

	public function index()
	{
		$data = [];
		$user_id = $this->session->userdata('ci_seesion_key')['user_id'];
		$my_answer = $this->stats->get_user_by_id($user_id);
		$data['title'] = "Home";
		$data['my_answer'] = !empty($my_answer) ? $my_answer : '';
		$this->load->view('home', $data);
	}
}
