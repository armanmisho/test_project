<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stats extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		isLoggedIn();
		$this->load->model('Stats_model', 'stats');
	}

	public function index()
	{
		$data = [];
		$data['title'] = "Stats";

		$answers = $this->stats->select_all();
		$data['answers'] = !empty($answers) ? $answers[0] : '';
		//$data['answers'] = !empty($answers) ? $answers[0] : '';
		$this->load->view('stats', $data);
	}

	public function save() {
		if(!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$answers = $this->input->post('answers');
		$user_id = $this->session->userdata('ci_seesion_key')['user_id'];

		//checking have is answer for current user
		$have = $this->stats->get_user_by_id($user_id);

		$data = array(
			"user_id" => $user_id
		);

		for($i = 1; $i < 11; $i++) {
			$data['answer_' . $i] = $answers[($i - 1)];
		}

		if(!empty($have)) {
			$this->db->update('stats', $data, "id=" . $have['id']);
		} else {
			$this->db->insert('stats', $data);
		}

		echo json_encode(['success' => 1]);
		exit;

	}
}
