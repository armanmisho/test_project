<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Auth extends CI_Controller {
 
    public function __construct() {
        parent::__construct();
        //load model
        $this->load->model('Auth_model', 'auth');
        $this->load->library('form_validation');
    }
 
    // index method
    public function registration() {        
        $data = array();
        $data['title'] = "Registration";
        
        $this->load->view('auth/registration', $data);
    }
 
    // action create user method
    public function actionRegister() {

        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[password]');
 
        if ($this->form_validation->run() == FALSE) {
            $this->registration();
        } else {
            $firstName = $this->input->post('first_name');
            $lastName = $this->input->post('last_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $isAdmin = $this->input->post('is_admin') && $this->input->post('is_admin') == '1' ? 'Y' : 'N';
      
            $this->auth->setFirstName(trim($firstName));
            $this->auth->setLastName(trim($lastName));
            $this->auth->setEmail($email);  
            $this->auth->setPassword($password);
            $this->auth->setIsAdmin($isAdmin);
            $chk = $this->auth->create();
            redirect('auth/login');
        }
    }
    
    // login method
    public function login() {        
        $data = [];
        $data['title'] = "Login";
        
        $this->load->view('auth/login', $data);
    }
    
 
    // action login method
    function doLogin() {        
        // Check form  validation
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $this->login();
        } else {
          $sessArray = array();
            //Field validation succeeded.  Validate against database
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            $this->auth->setEmail($email);
            $this->auth->setPassword($password);
            //query the database
            $result = $this->auth->login();
 
            if (!empty($result) && count($result) > 0) {
                foreach ($result as $row) {
                    $authArray = array(
                        'user_id' => $row->user_id,
                        'first_name' => $row->first_name,
                        'last_name' => $row->last_name,
                        'full_name' => $row->first_name . ' ' . $row->last_name,
                        'email' => $row->email,
                        'is_admin' => $row->is_admin,
                        'is_authenticate_user' => TRUE,
                    );
                    $this->session->set_userdata('ci_session_key_generate', TRUE);
                    $this->session->set_userdata('ci_seesion_key', $authArray);
                    // remember me
                    if(!empty($this->input->post("remember"))) {
                      setcookie ("loginId", $username, time()+ (10 * 365 * 24 * 60 * 60));  
                      setcookie ("loginPass", $password,  time()+ (10 * 365 * 24 * 60 * 60));
                    } else {
                      setcookie ("loginId",""); 
                      setcookie ("loginPass","");
                    }                    
                }

                redirect('home');
            } else {
            	$this->session->set_flashdata('login_error', 'Email or password incorrect.');
                $this->login();
            }
        }
    }
    
    //logout method
    public function logout() {
        $this->session->unset_userdata('ci_seesion_key');
        $this->session->unset_userdata('ci_session_key_generate');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('auth/login');
    }   
 
}
?>