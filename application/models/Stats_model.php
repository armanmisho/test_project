<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stats_model extends CI_Model {

    public function get_user_by_id($id = 0) {
        $query = $this->db->get_where('stats', array('user_id' => $id));
        return $query->row_array();
    }

    public function select_all() {
        $this->db->select_avg('answer_1');
        $this->db->select_avg('answer_2');
        $this->db->select_avg('answer_3');
        $this->db->select_avg('answer_4');
        $this->db->select_avg('answer_5');
        $this->db->select_avg('answer_6');
        $this->db->select_avg('answer_7');
        $this->db->select_avg('answer_8');
        $this->db->select_avg('answer_9');
        $this->db->select_avg('answer_10');
        $query = $this->db->get('stats');
        return $query->result();
    }

    public function get_list_of_users() {
    	$query = $this->db->query('SELECT users.id, users.first_name, users.last_name, users.email FROM users WHERE users.id NOT IN (SELECT user_id FROM stats WHERE user_id = users.id)');

    	return $query->result();
    }
}

?>