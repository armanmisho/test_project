<?php $this->load->view('templates/header');?>

<div class="container" style="min-height: 400px;">
    <h4 class="text-muted pb-5">List of users that registered but did not fill out the survey</h4>
    <div class="row">
        <div class="col-md-12 pb-4">
            <table class="table table-striped">
				<thead>
					<tr>
						<th>Firstname</th>
						<th>Lastname</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($list_of_users)): ?>
						<?php foreach($list_of_users as $user): ?>
							<tr>
								<td><?= $user->first_name; ?></td>
								<td><?= $user->last_name; ?></td>
								<td><?= $user->email; ?></td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td class="text-center" colspan="3">No Users</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
        </div>
    </div>

</div>

<?php $this->load->view('templates/footer');?>