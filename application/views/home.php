<?php $this->load->view('templates/header');?>

<div class="container mb-4">
	
	<ul class="breadcrumb">
		<li class="breadcrumb-item active">Previous</li>
		<li class="breadcrumb-item"><a href="javascript:void(0);">Next</a></li>
	</ul>

	<h3>Please choose answer for each question.</h3>

	<div id="success_message" class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong>Success!</strong> Data saved successfully.
	</div>
	
	<div id="first_form" class="row">
		<div class="col-sm-12">
			<?php for($i = 1; $i < 6; $i++): ?>
				<div class="form-group">
					<label>Question <?= $i; ?>:</label>
					<select class="form-control select_question">
						<?php for($j = 1; $j < 6; $j++): ?>
							<?php 
								$selected = '';
								if(!empty($my_answer) && $my_answer['answer_' . $i] == $j) {
									$selected = 'selected';
								}
							?>
							<option value="<?= $j; ?>" <?= $selected; ?> ><?= $j; ?></option>
						<?php endfor; ?>
					</select>
				</div>
			<?php endfor; ?>
		</div>
	</div>

	<div id="second_form" class="row">
		<div class="col-sm-12">
			<?php for($i = 6; $i < 11; $i++): ?>
				<div class="form-group">
					<label>Question <?= $i; ?>:</label>
					<select class="form-control select_question">
						<?php for($j = 1; $j < 6; $j++): ?>
							<?php 
								$selected = '';
								if(!empty($my_answer) && $my_answer['answer_' . $i] == $j) {
									$selected = 'selected';
								}
							?>
							<option value="<?= $j; ?>" <?= $selected; ?> ><?= $j; ?></option>
						<?php endfor; ?>
					</select>
				</div>
			<?php endfor; ?>
			
			<button class="btn btn-primary mt-2 mb-3" id="save_btn">Save</button>
		</div>

	</div>
</div>

<?php $this->load->view('templates/footer');?>        