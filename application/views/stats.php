<?php $this->load->view('templates/header');?>

<div class="container">
    
    <div class="row">
        <div class="col-md-6 pb-4">
            <h4>Statistics</h4>
            <canvas id="myChart"></canvas>
        </div>

        <div class="col-md-6 pb-4">
            <h4>Statistics</h4>
            <canvas id="myChart_2"></canvas>
        </div>
    </div>

</div>

<script>
    var answers = JSON.parse('<?= json_encode($answers); ?>');
    console.log(answers)
</script>

<?php $this->load->view('templates/footer');?>