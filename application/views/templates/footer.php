<!-- Footer -->
  <footer class="footer bg-dark footer-bg-dark p-5">
    <div class="container">
        <p style="color: #ffffff" class="text-center">Copyright &copy;  2017 - <?= date('Y');?> All rights reserved.</p>
    </div>
  </footer>
  <!-- Bootstrap core JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
  <?php if($this->uri->segment(1) == 'home' || $this->uri->segment(1) == ''): ?>
    <script src="<?= base_url('assets/js/home.js'); ?>"></script>
  <?php endif;?>
  <?php if($this->uri->segment(1) == 'stats'): ?>
    <script src="<?= base_url('assets/js/stats.js'); ?>"></script>
  <?php endif;?>
  <script>
    var base_url = "<?= base_url(); ?>";
  </script>
</body>
</html>   