<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= $title; ?></title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
  <!-- Custom fonts for this template -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template -->
  <?php if($this->uri->segment(1) == 'home' || $this->uri->segment(1) == ''): ?>
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
  <?php endif;?>
  
</head>
<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top header-bg-dark">
  <div class="container">
    <a class="navbar-brand font-weight-bold" href="<?= base_url(); ?>"><h1>LOGO</h1></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item <?= $this->uri->segment(1) == 'home' || $this->uri->segment(1) == '' ? 'active' : ''; ?>">
          <a class="nav-link" href="<?= base_url(); ?>">Home</a>
        </li>

        <?php if( $this->session->userdata('ci_seesion_key')['is_authenticate_user'] != FALSE ): ?>

          <?php if( $this->session->userdata('ci_seesion_key')['is_admin'] == 'Y' ): ?>
            <li class="nav-item <?= $this->uri->segment(1) == 'admin' ? 'active' : ''; ?>">
              <a class="nav-link" href="<?= base_url('admin'); ?>">Admin</a>
            </li>
          <?php endif; ?>

          <li class="nav-item <?= $this->uri->segment(1) == 'stats' ? 'active' : ''; ?>">
            <a class="nav-link" href="<?= base_url('stats'); ?>">Stats</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('auth/logout'); ?>">Log Out</a>
          </li>
        <?php else: ?>
          <li class="nav-item <?= $this->uri->segment(2) == 'login' ? 'active' : ''; ?>">
            <a class="nav-link" href="<?= base_url('auth/login'); ?>">Login</a>
          </li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>

<?php if( $this->session->userdata('ci_seesion_key')['is_authenticate_user'] != FALSE ): ?>
  <div class="container">
    <p class="text-right"><?= $this->session->userdata('ci_seesion_key')['full_name']; ?></p>
  </div>
<?php endif; ?>