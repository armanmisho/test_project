<?php
	defined('BASEPATH') OR exit('no direct access');

	function isLoggedIn()
	{
		if($_SESSION['ci_seesion_key']['is_authenticate_user'] == FALSE) {
			redirect(base_url('auth/login'));
		}
	}

?>